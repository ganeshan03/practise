#include <iostream>
using namespace std;
int main()
{
    double total_amount, discount, actual_price;
    cout << "Enter the total bill amount";
    cin >> total_amount;
    if (total_amount >= 100 && total_amount < 500)
    {
        discount = total_amount * 0.1;
        actual_price = total_amount - discount;
        cout << "cash to pay = " << actual_price << endl;
    }
    else if (total_amount >= 500)
    {
        discount = total_amount * 0.2;
        actual_price = total_amount - discount;
        cout << "cash to pay = " << actual_price << endl;
    }
    else
    {
        cout << "No discount available to you\n";
        cout << "cash to pay " << total_amount << endl;
    }
    return 0;
}