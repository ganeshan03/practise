#include <iostream>
using namespace std;
int main()
{
    int a[10][10], b[10][10], mult[10][10], r1, c1, r2, c2, i, j, k;
    cout << "Enter rows and columns for first matrix: ";
    cin >> r1 >> c1;
    cout << "Enter rows and columns for second matrix: ";
    cin >> r2 >> c2;
    //condition statement for checking rows and coloumns 
    if (r2!=c1)
    {
        cout<<"give the proper rows and coloum values"<<endl;
        cout << "Enter rows and columns for first matrix: ";
        cin >> r1 >> c1;
        cout << "Enter rows and columns for second matrix: ";
        cin >> r2 >> c2;
    }
    //given and store the first matrix
    cout << endl << "Enter elements of matrix 1:" << endl;
    for (i = 1; i <= r1; i++)
        for (j = 1; j <= c1; j++)
        {
            cout << "Enter element a" << i << j << " : ";
            cin >> a[i][j];
        }
    //given and store the second matrix
    cout << endl << "Enter elements of matrix 2:" << endl;
    for (i = 1; i <= r2; i++)
        for (j = 1; j <= c2; j++)
        {
            cout << "Enter element b" << i << j << " : ";
            cin >> b[i][j];
        }
    //Multiplication initialization
    for (i = 1; i <= r1; i++)
        for (j = 1; j <= c2; j++)
        {
            mult[i][j] = 0;
            for (k = 1; k <= c1; k++)
            {
                mult[i][j] += a[i][k] * b[k][j];
            }
        }

    cout << endl << "Output Matrix: " << endl;
    for (i = 1; i <= r1; i++)
        for (j = 1; j <= c2; j++)
        {
            cout << " " << mult[i][j];
            if (j == c2) // putting the values in coloum wise
                cout << endl;
        }
    return 0;
}