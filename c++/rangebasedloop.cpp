//concept reference :https://docs.microsoft.com/en-us/cpp/cpp/range-based-for-statement-cpp?view=vs-2017
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int main()
{
    vector<int> v{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    for (auto &y : v)
    {
        cout << y << " ";
    } 
    cout<<endl;

    return 0;
}
