#include <iostream>
using namespace std;
int main()
{
    int A[3][2] = {3, 4, 3, 4, 3, 3};
    int B[3][2] = {3, 4, 3, 4, 3, 5};
    int C[3][2];
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 2; j++)
        {
            C[i][j] = A[i][j] + B[i][j];
        }
    }
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 2; j++)
        {
            cout << C[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}