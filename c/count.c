//count the number of digits present in user's input
#include <stdio.h>
void main()
{
    unsigned long n;
    int count = 0;
    printf("enter the integer");
    scanf("%llu",&n);
    while(n!=0)
    {
        n/=10;
        count++;
    }
    printf("integer digits = %d\n",count);
}