#include <stdio.h>
char str1[100];//global declaration
char *fun(char str[])
{
    static int i = 0;
    if (*str)
    {
        fun(str+1);
        str1[i] = *str;//String reverse operation performed here
        i++;
    }
    return str1;
}
 
int main()
{
    char str[] = "Ganeshan";
    printf("%s", fun(str));
    return 0;
}