//swapping two numbers using call by value
#include <stdio.h>
void swap(int*, int*);
void main()
{
    int x,y;
    printf("enter swapping number ");
    scanf("%d%d",&x,&y);
    swap(&x,&y);
    printf("after swapping x = %d\t , y = %d\t ",x,y);
} 
void swap(int*a, int*b)
{
    int temp;
    temp = *b;
    *b=*a;
    *a=temp;
}