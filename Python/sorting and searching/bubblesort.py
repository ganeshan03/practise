def bubblesort(items):
    i = 0
    u = len(items)-1
    loop = True
    while(u > 0 and loop):
        loop = False
        for i in range(u):
            if items[i] > items[i+1]:
                (items[i], items[i+1]) = (items[i+1], items[i])
                loop = True
        u -= 1

items = []
n = int(input("enter the size of the list "))
for i in range(n):
    element = int(input("enter the items"))
    items.append(element)
print("element in the list is ", items)
bubblesort(items)
print(items)
