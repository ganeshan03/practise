
def ganesh(items, x, n):
    for i in range(0, n):
        if items[i] == x:
            return i
    return -1


n = int(input("enter size of list "))
items = []
for i in range(n):
    element = int(input("enter the list item "))
    items.append(element)

print("list of items is", items)
x = int(input("enter item to search:"))
result = ganesh(items, x, n)
if result != -1:
    print("element found at position:", result +
          1, "element is", items[result])
else:
    print(" given ", x, "element not found ")
