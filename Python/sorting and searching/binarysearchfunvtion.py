
def ganesh (items,x):
    l = 0
    u = len(items)-1
    while l <= u :
        m = (l+u) // 2
        if x == items[m]:
            return m
        elif x < items[m]:
            u = m - 1
        else:
            l =  m + 1
    return -1


n = int(input("enter size of list "))
items = []
for i in range(n):
    element = int(input("enter the list item "))
    items.append(element)
items.sort()
print("list of items is", items)
x = int(input("enter item to search:"))
result = ganesh(items,x)
if result != -1:
    print("element is found at position ",result+1," and element is ",items[result])
else:
    print("given ",x,"not found")