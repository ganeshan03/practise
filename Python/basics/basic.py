# python is fun
#working with power of (**)
2 ** 3 #it gives 8

# it gives quotient
10 // 3
# op = 3


# it gives remainder
10 %  3
# op = 1



#helloworld program
print("Hello World")

#strings 
firstname = "ganeshan"
lastname = "NT"
fullname = firstname+lastname
print (fullname)


firstname = "ganeshan"
lastname = "NT"
fullname = firstname+" "+lastname
print (fullname)


#Random operation and string manipulation

ganesh = "ganeshanisadatascientistand"
print (ganesh)
print (ganesh[1]) # print the first character only
print (ganesh [0:8])#print the starting and ending number of character
print (ganesh [8:]) # print rest of characters and it is starting with 8th character
print (ganesh [:7])#print till 7th character


#Topic : LIST - In list we use - []
myfriends=["akshay","ragul","mano","aj"]
print (myfriends[1])
#delete LIST of an element 
del myfriends[3]
print (myfriends)
#lenth of LIST
print (len(myfriends))
#add element into LIST
#Append take only one argument at a time
myfriends.append("i don't know")
print myfriends
#change item into the list
myfriends[2]="newbie added"
print myfriends
#COUNT - how many time one item repeated into the LIST
myfriends.append("akshay")
print myfriends
print myfriends.count("akshay")



#ARRAY - In array we use - []
array1 = ["20","30","40"]
array2 = ["50","60","70","80","90","1002"]
array3 = array1+array2
print array3
#maximum and minimum size of element in array
print max(array2)
print max(array3)
print min(array3)


#DICTIONARIES - In dictionary we use - {}
#name and cooresponding student mark put into dictionaries
mydic = {"ganesh":100,"akshay":99,"mano":98}
#"ganesh" - is a key and 100 is a key value in dictionary
print mydic["ganesh"]
#change value into dictionaries
mydic["akshay"]=100
print mydic["akshay"]
#Delete dictionaries
del mydic["akshay"]
print mydic
#lenth of the dictionary
print len(mydic)
#keys of dictionary
mydic.keys()
print mydic.keys
print mydic.keys()
#CLEAR and DELETE Dictionary
mydic2 = {"apple":20,"orange":50,"lemon":5}
print mydic2
mydic2.clear()#it is used to clear all the keys and values of dictionary but it is not delete the dictionary
print mydic2
del mydic2
#VALUES of dictionary
myfruits1 = {"apple":20,"orange":50,"lemon":5}
print myfruits1.values()#it shows the key values of dictionary
#UPDATE dictionaries
myfruits2 = {"tomato":60,"hotchips":50,"watermelon":5}
print myfruits1
print myfruits2
myfruits1.update(myfruits2)
print myfruits1






#TUPLES - tuples and dictionary both are similar but couldnot change values in tuples . once ou created the tuple after you cannot change the value of tuple
#TUPLES are immuttable
#TUPLES - In tuples we use - ()
tup = ("tamil",97,"english",799,98)
print tup
#concept of silaicing
print tup[0]
print tup[0:2]
print tup[1:]
tup1 = ("tamil",799,98)
#delete tuple
del tup1










