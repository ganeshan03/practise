class student:
    college = "Paavai"

    def __init__(self,m1,m2,m3):
        self.m1 = m1
        self.m2 = m2
        self.m3 = m3
    
    def avg(self):
        return (self.m1+self.m2+self.m3)/3

    @classmethod
    def getcollege(cls):
        return cls.college


    @staticmethod
    def info():
        print("This is student class ")

ganeshan = student(80,90,80)
akshay = student(100,90,88)

print(ganeshan.avg())
print(student.getcollege())
student.info() 