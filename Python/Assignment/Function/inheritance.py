class A:
    def __init__(self):
        print("it is A init")
    
    def feature1(self):
        print("feature 1 is working...")

class B():

    def __init__(self):
        super().__init__()
        print("it is B init")

    def feature2(self):
        print("feature 2 is working...")

    def feature3(self):
        print("feature 3 is working...")


# multiple inheritance goes left to right values

class C(A,B):

    def __init__(self):
        super().__init__()
        print("it is C init")

    def feature4(self):
        print("feature 4 is working...")

    def feature5(self):
        print("feature 5 is working...")

feature = C()
feature.feature1()
