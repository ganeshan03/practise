from time import sleep
from threading import *

class action1(Thread):
    def run(self):
        for i in range(5):
            print("first eat well.....",i)
            sleep(1)

class action2(Thread):
    # inbulid method to initiate the thread
    def run(self):
        for i in range(5):
            print("     then study well.....",i)
            sleep(1)

A1 = action1()
A2 = action2()

A1.start()
# take .2 sec to take action 
sleep(0.2)

A2.start()

# progress one by one from eact thread's function parallelly
A1.join()
A2.join()

print('bye')



# a = 6
# b = 5
# try:
#     print(a/b)
# except Exception as e:
#     print(e.__class__.__name__, "occurred")