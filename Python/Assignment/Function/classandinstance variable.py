class lorry:
    # class varible
    wheels = 10
    def __init__(self):
    # instance variable
        self.model = 2010
        self.milage = 5
        self.engine = "leyland"
        self.manufacturer = "ashok leyland"

# L1,L2 are objects of car. wheels shared by both objects but instace varible make shallow copy of itself in the time of object creation
L1 = lorry() 
L2 = lorry()
L1.manufacturer = "tata"
L1.milage = 4.5
print(L1.model,L1.milage,L1.wheels,L1.manufacturer)
print(L2.model,L2.milage,L2.wheels,L2.manufacturer)
