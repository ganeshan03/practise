
# examole of duck typing (polymorphism)
class vscode:
    def execute(self):
        print("it is compile")
        print("it is running")

class myeditor:
    def execute(self): 
        print("spell check")
        print("convention check")

class laptop:
    def code(self,ide):
        ide.execute()

ide = myeditor()
lap1 = laptop()
lap1.code(ide)
