class student:

    def __init__(self,name,rollno):
        self.name = name
        self.rollno = rollno
        # innner class representation in outer class
        self.lap = self.laptop()
    
    def show(self):
        print(self.name,self.rollno)
        self.lap.show()  
    
    # inner class
    class laptop:

        def __init__(self):
            self.brand = 'dell'
            self.cpu = 'i5'
            self.ram = 8
        
        def show(self):
            print(self.brand, self.cpu,self.ram)

s1 = student('ganeshan',15)
s2 = student('akshay',5)
s1.show()


    