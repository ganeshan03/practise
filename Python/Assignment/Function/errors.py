
a = 6
b = 0

try:
    print('connection open')
    print(a/b)

except ZeroDivisionError as e:
    print('zero division error')
except ValueError as e:
    print('value error')

except Exception as e:
    print('something went wrong..........')

finally:
    print('connection closed')
















# bestway to handle the errors
# a = 6
# b = 0
# try:
#     print(a/b)
# except Exception as e:
#     print(e.__class__.__name__, "occurred")
